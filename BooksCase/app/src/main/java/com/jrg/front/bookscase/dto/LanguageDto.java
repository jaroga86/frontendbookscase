package com.jrg.front.bookscase.dto;

public class LanguageDto {


    private String language;

    public LanguageDto(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
