package com.jrg.front.bookscase.entity;

import com.jrg.front.bookscase.entity.segurity.entity.User;

import java.util.ArrayList;
import java.util.List;

public class CartInfo  {

    private User user;
    private List<CartLineInfo> cartLines = new ArrayList<CartLineInfo>();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CartLineInfo> getCartLines() {
        return cartLines;
    }

    public CartLineInfo findLineByISBN(long isbn) {
        for (CartLineInfo line : this.cartLines) {
            if (line.getBook().getIsbn() == isbn) {
                return line;
            }
        }
        return null;
    }

    public Boolean isFindLineByISBN(long isbn) {
        for (CartLineInfo line : this.cartLines) {
            if (line.getBook().getIsbn() == isbn) {
                return true;
            }
        }
        return false;
    }

    public void addBook(Book book, int quantity) {
        CartLineInfo line = this.findLineByISBN(book.getIsbn());

        if (line == null) {
            line = new CartLineInfo();
            line.setQuantity(0);
            line.setBook(book);
            this.cartLines.add(line);
        }
        int newQuantity = line.getQuantity() + quantity;
        if (newQuantity <= 0) {
            this.cartLines.remove(line);
        } else {
            line.setQuantity(newQuantity);
        }
    }

    public void updateBook(Long isbn, int quantity) {
        CartLineInfo line = this.findLineByISBN(isbn);

        if (line != null) {
            if (quantity <= 0) {
                this.cartLines.remove(line);
            } else {
                line.setQuantity(quantity);
            }
        }
    }

    public void removeProduct(Book book) {
        CartLineInfo line = this.findLineByISBN(book.getIsbn());
        if (line != null) {
            this.cartLines.remove(line);
        }
    }

    public int getQuantityTotal() {
        int quantity = 0;
        for (CartLineInfo line : this.cartLines) {
            quantity += line.getQuantity();
        }
        return quantity;
    }

    public float getTotalPrice() {

        float total = 0.0f;
        for (CartLineInfo line : this.cartLines) {
            total += line.getQuantity() * line.getBook().getPrice();
        }
        return total;
    }
}
