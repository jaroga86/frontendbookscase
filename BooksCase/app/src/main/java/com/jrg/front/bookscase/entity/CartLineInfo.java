package com.jrg.front.bookscase.entity;

import com.jrg.front.bookscase.entity.Book;

public class CartLineInfo {

    private Book book;
    private int quantity;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
