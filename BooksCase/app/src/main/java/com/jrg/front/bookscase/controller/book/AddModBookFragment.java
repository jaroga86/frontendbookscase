package com.jrg.front.bookscase.controller.book;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentNewBookBinding;
import com.jrg.front.bookscase.entity.Author;
import com.jrg.front.bookscase.entity.Book;
import com.jrg.front.bookscase.entity.Category;
import com.jrg.front.bookscase.entity.Language;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.dto.BookDto;
import com.jrg.front.bookscase.dto.Mensaje;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddModBookFragment extends Fragment {

    private FragmentNewBookBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private List<Author> listaAuthor = new ArrayList<>();
    private List<Category> listaCategory = new ArrayList<>();
    private List<Language> listaLanguage = new ArrayList<>();
    private ActivityResultLauncher<Intent> selectImagePortada;
    private Language language;
    private Author  author;
    private Category category;
    private Uri path;
    private Book book;
    private ArrayList<String> authorsName = new ArrayList<>();
    private ArrayList<String> categoriesName = new ArrayList<>();
    private ArrayList<String> languagesName = new ArrayList<>();
    private Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentNewBookBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());
        apiService = Apis.getClient();
        fillSpAuthor(root);
        fillSpCategory(root);
        fillSpLanguage(root);


        bundle = getArguments();


        if(bundle != null){
            book = (Book) bundle.getSerializable("book");
            binding.editIsbn.setFocusable(false);
            binding.editIsbn.setClickable(false);
            binding.editIsbn.setCursorVisible(false);
            binding.editIsbn.setTextColor(getResources().getColor(R.color.grey));
            binding.editIsbn.setText(String.valueOf(book.getIsbn()));
            binding.editName.setText(book.getName());
            binding.editPag.setText(String.valueOf(book.getN_pag()));
            binding.editDesc.setText(book.getSummary());
            binding.editBinding.setText(book.getBinding());
            binding.editPrice.setText(String.valueOf(book.getPrice()));
            binding.editStock.setText(String.valueOf(book.getStock()));
            Glide.with(root).load(Apis.getUrlImg(book.getPath_img())).into(binding.subirImage);
            binding.idGuardarBook.setVisibility(Button.INVISIBLE);
        }else{
            binding.idModificarBook.setVisibility(Button.INVISIBLE);
            binding.idEliminarBook.setVisibility(Button.INVISIBLE);
        }

        selectImagePortada = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                            // There are no request codes
                            Intent data = result.getData();
                            path = data.getData();
                            binding.subirImage.setImageURI(path);
                        }
                    }
                });












        binding.spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItemText = (String) adapterView.getItemAtPosition(i);

                Call<Language>  call = apiService.getLanguage(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), selectedItemText);

                call.enqueue(new Callback<Language>() {
                    @Override
                    public void onResponse(Call<Language> call, Response<Language> response) {
                        language = response.body();
                    }

                    @Override
                    public void onFailure(Call<Language> call, Throwable t) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String selectedItemText = (String) adapterView.getItemAtPosition(i);
                Call<Category>  call = apiService.getCategory(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), selectedItemText);
                call.enqueue(new Callback<Category>() {
                    @Override
                    public void onResponse(Call<Category> call, Response<Category> response) {
                        category = response.body();
                    }

                    @Override
                    public void onFailure(Call<Category> call, Throwable t) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.spAuthor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItemText = (String) adapterView.getItemAtPosition(i);

                String[] authors = selectedItemText.split(", ");

                Call<Author>  call = apiService.getAuthor(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), authors[0], authors[1]);

                call.enqueue(new Callback<Author>() {
                    @Override
                    public void onResponse(Call<Author> call, Response<Author> response) {
                        author = response.body();
                    }

                    @Override
                    public void onFailure(Call<Author> call, Throwable t) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        binding.subirImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                selectImagePortada.launch(intent);
            }
        });



        binding.idGuardarBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(getPathImg()!=null) {


                    BookDto bookDto = new BookDto();
                    bookDto.setIsbn(Long.valueOf(binding.editIsbn.getText().toString()));
                    bookDto.setName(binding.editName.getText().toString());
                    bookDto.setN_pag(Integer.valueOf(binding.editPag.getText().toString()));
                    bookDto.setSummary(binding.editDesc.getText().toString());
                    bookDto.setBinding(binding.editBinding.getText().toString());
                    bookDto.setPrice(Float.valueOf(binding.editPrice.getText().toString()));
                    bookDto.setStock(Integer.valueOf(binding.editStock.getText().toString()));
                    bookDto.setPath_img(getPathImg());
                    bookDto.setLanguage(language);
                    Set<Author> authors = new HashSet<Author>();
                    authors.add(author);
                    bookDto.setAuthors(authors);
                    Set<Category> categories = new HashSet<Category>();
                    categories.add(category);
                    bookDto.setCategories(categories);

                    Call<Mensaje> callBook = apiService.postBook(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), bookDto);

                    callBook.enqueue(new Callback<Mensaje>() {
                        @Override
                        public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                            responseClient(response);
                        }

                        @Override
                        public void onFailure(Call<Mensaje> call, Throwable t) {

                        }
                    });
                }else{
                    Toast.makeText(getContext().getApplicationContext(), "No se ha cargado la imagen de la portada.", Toast.LENGTH_SHORT).show();
                }

                }


        });

        binding.idModificarBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                BookDto bookDto = new BookDto();
                bookDto.setIsbn(Long.valueOf(binding.editIsbn.getText().toString()));
                bookDto.setName(binding.editName.getText().toString());
                bookDto.setN_pag(Integer.valueOf(binding.editPag.getText().toString()));
                bookDto.setSummary(binding.editDesc.getText().toString());
                bookDto.setBinding(binding.editBinding.getText().toString());
                bookDto.setPrice(Float.valueOf(binding.editPrice.getText().toString()));
                bookDto.setStock(Integer.valueOf(binding.editStock.getText().toString()));
                bookDto.setLanguage(language);
                Set<Author> authors = new HashSet<Author>();
                authors.add(author);
                bookDto.setAuthors(authors);
                Set<Category> categories = new HashSet<Category>();
                categories.add(category);
                bookDto.setCategories(categories);

                if(getPathImg()!=null){
                    bookDto.setPath_img(getPathImg());
                }else{
                    bookDto.setPath_img(book.getPath_img());
                }

                Call<Mensaje> call = apiService.updateBook(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                        book.getIsbn(), bookDto);

                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                        responseClient(response);
                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });

            }
        });

        binding.idEliminarBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Mensaje> call = apiService.deleteBook(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), book.getIsbn());

                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                        responseClient(response);
                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });
            }
        });




        return root;
    }

    private String getPathImg() {

        if(path!= null) {

            File imageFile = new File(path.getPath().replace("/raw", ""));
            RequestBody reqBody = RequestBody.create(MediaType.parse("multipart/form-file"), imageFile);
            MultipartBody.Part partImage = MultipartBody.Part.createFormData("file", imageFile.getName(), reqBody);
            Call<Mensaje> call = apiService.uploadImage(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), partImage);

            call.enqueue(new Callback<Mensaje>() {
                @Override
                public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                    Mensaje mensaje = response.body();

                    Log.d("newBook", mensaje.getMensaje());

                }

                @Override
                public void onFailure(Call<Mensaje> call, Throwable t) {

                    Log.e("newBook", "Revisa los permisos: " + t.getMessage());
                    Toast.makeText(getContext().getApplicationContext(), "Revisa los permisos necesita acceder archivos y contenido multimedia.", Toast.LENGTH_SHORT).show();

                }
            });

            return imageFile.getName();

        }else{
            return null;
            }
    }

    private void fillSpLanguage(View root) {

        Call<List<Language>> call2 = apiService.getLanguages(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken());
        call2.enqueue(new Callback<List<Language>>() {
            @Override
            public void onResponse(Call<List<Language>> call, Response<List<Language>> response) {
                listaLanguage = response.body();

                for(Language language: listaLanguage){
                    languagesName.add(language.getLanguage());
                }
                binding.spLanguage.setAdapter(new ArrayAdapter<>(root.getContext(), android.R.layout.simple_spinner_dropdown_item, languagesName));
                if(bundle != null){
                    binding.spLanguage.setSelection(languagesName.indexOf(book.getLanguage().getLanguage()));
                }
            }
            @Override
            public void onFailure(Call<List<Language>> call, Throwable t) {
                Log.e("ListaLenguajes:", t.getMessage());
            }
        });
    }

    protected void fillSpCategory(View root) {

        Call<List<Category>> call1 = apiService.getCategories(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken());
        call1.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                listaCategory = response.body();

                for(Category category: listaCategory){
                    categoriesName.add(category.getName());
                }

                binding.spCategory.setAdapter(new ArrayAdapter<>(root.getContext(), android.R.layout.simple_spinner_dropdown_item, categoriesName));
                if(bundle != null){
                    binding.spCategory.setSelection(categoriesName.indexOf(book.getCategories().iterator().next().getName()));
                }
            }
            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.e("ListaCategorias:", t.getMessage());
            }
        });
    }

    protected void fillSpAuthor(View root) {

        Call<List<Author>> call = apiService.getAuthors(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken());

        call.enqueue(new Callback<List<Author>>() {
            @Override
            public void onResponse(Call<List<Author>> call, Response<List<Author>> response) {
                listaAuthor = response.body();


                for(Author author: listaAuthor){
                    authorsName.add(author.getName() + ", " + author.getSurnames());
                }

                binding.spAuthor.setAdapter(new ArrayAdapter<>(root.getContext(), android.R.layout.simple_spinner_dropdown_item, authorsName));
                if(bundle != null){
                    binding.spAuthor.setSelection(authorsName.indexOf(book.getAuthors().iterator().next().getName() + ", " + book.getAuthors().iterator().next().getSurnames()));
                }
            }
            @Override
            public void onFailure(Call<List<Author>> call, Throwable t) {
                Log.e("ListaAutores:", t.getMessage());
            }
        });
    }


    protected void responseClient(Response<Mensaje> response) {

        Mensaje mensaje;

        if (!response.isSuccessful() && response.code() == 400) {
            try {
                Gson gson = new Gson();
                String jsonError = response.errorBody().string();
                mensaje = gson.fromJson(jsonError, Mensaje.class);
                Toast.makeText(getContext().getApplicationContext(), Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();
            } catch (IOException io) {
                io.printStackTrace();
            }

        } else if (response.code() == 200) {

            mensaje = response.body();


            Toast.makeText(getContext().getApplicationContext(), Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();

            NavHostFragment.findNavController(AddModBookFragment.this)
                    .navigate(R.id.action_newBook_to_allBook);
        }


    }

}