package com.jrg.front.bookscase.dto;

import com.jrg.front.bookscase.entity.segurity.entity.User;

public class JwtDto {

    private String token;
    private String bearer = "Bearer";
    private User user;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBearer() {
        return bearer;
    }

    public void setBearer(String bearer) {
        this.bearer = bearer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
