package com.jrg.front.bookscase.controller.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.ActivityMainBinding;
import com.jrg.front.bookscase.databinding.FragmentHomeBinding;
import com.jrg.front.bookscase.entity.Book;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.entity.CartInfo;
import com.jrg.front.bookscase.entity.segurity.entity.User;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Apis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment{


    private FragmentHomeBinding binding;

    private List<Book> listaBook = new ArrayList<>();
    private List<Book> listaBookSearch = new ArrayList<>();
    private APIService apiService;

    //Libros principales
    private ImageView imageViewPortada;
    private TextView textViewAuthor;
    private TextView textViewTitle;
    private TextView textViewPrice;
    //Libros disponibles
    private TextView bookAvailable;
    //Novedades
    private ImageView novPortada;
    private TextView novTitle;
    private TextView novAuthor;
    private TextView novPrice;
    private ActivityMainBinding bindingActivity;
    private CartInfo cartInfo;



    SharedPrefManager sharedPrefManager;




    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        getBooks(root);

        sharedPrefManager = new SharedPrefManager(getContext().getApplicationContext());

        if(sharedPrefManager.isLoggedIn()){

            getActivity().invalidateOptionsMenu();
        }

        if(sharedPrefManager.getCartInfo() != null){
            cartInfo = sharedPrefManager.getCartInfo();

            binding.idNumCart.setText(String.valueOf(cartInfo.getQuantityTotal()));
            Log.d("carrito", "cantidad " + cartInfo.getQuantityTotal());
        }else{
            cartInfo = new CartInfo();
            sharedPrefManager.saveCart(cartInfo);
        }


        binding.idCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(sharedPrefManager.isLoggedIn()){

                    User user = sharedPrefManager.getUser().getUser();

                    CartInfo cartInfo = sharedPrefManager.getCartInfo();

                    cartInfo.setUser(user);

                    sharedPrefManager.saveCart(cartInfo);

                    NavHostFragment.findNavController(HomeFragment.this)
                            .navigate(R.id.action_nav_home_to_cart);

                }else{
                    NavHostFragment.findNavController(HomeFragment.this)
                            .navigate(R.id.action_nav_home_to_nav_login);
                }
            }
        });







        binding.idbook1portada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idbook1portada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);

            }
        });
        binding.idbook2portada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idbook2portada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);

            }
        });
        binding.idbook3portada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idbook3portada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);

            }
        });
        binding.idbook4portada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idbook4portada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);

            }
        });
        binding.idbook5portada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idbook5portada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);

            }
        });
        binding.idbook6portada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idbook6portada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);

            }
        });
        binding.idbook7portada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idbook7portada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);

            }
        });
        binding.idbook8portada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idbook8portada.getContentDescription());
                Log.d("HomeFragment", "ISBN" + (String) binding.idbook8portada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);
            }
        });

        binding.idBntNovDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) binding.idNovPortada.getContentDescription());
                Log.d("HomeFragment", "ISBN" + (String) binding.idNovPortada.getContentDescription());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_desc_book, args);

            }
        });

        binding.idVerTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("books", (Serializable) listaBook);
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_allBook, bundle);
            }
        });

        binding.idSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {


                int longitud = s.length();



                if(longitud == 0){
                    listaBookSearch.clear();
                    listaBookSearch.addAll(listaBook);

                }else{
                    for(Book book: listaBook){
                        if(book.getName().toLowerCase().contains(s.toLowerCase()) || String.valueOf(book.getIsbn()).contains(s)){
                            listaBookSearch.add(book);
                        }
                    }


                    Bundle bundle = new Bundle();
                    bundle.putSerializable("books", (Serializable) listaBookSearch);
                    NavHostFragment.findNavController(HomeFragment.this)
                            .navigate(R.id.action_nav_home_to_allBook, bundle);

                }






                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });



        return root;
    }

    private void getBooks(View root){
        apiService = Apis.getClient();
        Call<List<Book>> call = apiService.getBooks();

        call.enqueue(new Callback<List<Book>>() {
            @Override
            public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
                listaBook = response.body();
                bookAvailable = (TextView) root.findViewById(R.id.idLibrosDisponibles);
                bookAvailable.setText(listaBook.size() + " Libros Disponibles");
                fillBook(root);
                fillNovedades(root);
            }

            @Override
            public void onFailure(Call<List<Book>> call, Throwable t) {
                Log.e("Error:", t.getMessage());
            }
        });

    }



    private void fillNovedades(View root){
        int numBooksRnd = (int) Math.floor(Math.random()*(listaBook.size()-0)+0);

        novPortada = (ImageView) root.findViewById(R.id.idNovPortada);
        novTitle = (TextView) root.findViewById(R.id.idNovTitle);
        novAuthor = (TextView) root.findViewById(R.id.idNovAuthor);
        novPrice = (TextView) root.findViewById(R.id.idNovPrice);

        Glide.with(root).load(Apis.getUrlImg(listaBook.get(numBooksRnd).getPath_img())).into(novPortada);
        novPortada.setContentDescription(String.valueOf(listaBook.get(numBooksRnd).getIsbn()));
        novAuthor.setText(listaBook.get(numBooksRnd).getAuthors().iterator().next().getName() + " " + listaBook.get(numBooksRnd).getAuthors().iterator().next().getSurnames());
        novTitle.setText(listaBook.get(numBooksRnd).getName());
        novPrice.setText(String.valueOf(listaBook.get(numBooksRnd).getPrice()) + "€");

    }

    private void fillBook(View root){

        int numBooksRnd = 0;
        for(int i = 1; i <= 8; i++ ){

            int imageViewPortadaId = root.getResources().getIdentifier("idbook" + i + "portada", "id", root.getContext().getPackageName());
            int textViewAuthorId = root.getResources().getIdentifier("idbook" + i + "author", "id", root.getContext().getPackageName());
            int textViewTitleId = root.getResources().getIdentifier("idbook" + i + "title", "id", root.getContext().getPackageName());
            int textViewPriceId = root.getResources().getIdentifier("idbook" + i + "price", "id", root.getContext().getPackageName());

            imageViewPortada = (ImageView) root.findViewById(imageViewPortadaId);
            textViewAuthor = (TextView) root.findViewById(textViewAuthorId);
            textViewTitle = (TextView) root.findViewById(textViewTitleId);
            textViewPrice = (TextView) root.findViewById(textViewPriceId);
            imageViewPortada.setContentDescription(String.valueOf(listaBook.get(numBooksRnd).getIsbn()));
            Glide.with(root).load(Apis.getUrlImg(listaBook.get(numBooksRnd).getPath_img())).into(imageViewPortada);
            textViewAuthor.setText(listaBook.get(numBooksRnd).getAuthors().iterator().next().getName() + " " + listaBook.get(numBooksRnd).getAuthors().iterator().next().getSurnames());
            textViewTitle.setText(listaBook.get(numBooksRnd).getName());
            textViewPrice.setText(String.valueOf(listaBook.get(numBooksRnd).getPrice()) + "€");
            numBooksRnd++;
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}