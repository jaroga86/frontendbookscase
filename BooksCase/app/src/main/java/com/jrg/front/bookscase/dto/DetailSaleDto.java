package com.jrg.front.bookscase.dto;

import com.jrg.front.bookscase.entity.Book;
import com.jrg.front.bookscase.entity.Sales;

public class DetailSaleDto {


    private Sales sale;
    private Book book;
    private int quantity;
    private float price;

    public DetailSaleDto(Sales sale, Book book, int quantity, float price) {
        this.sale = sale;
        this.book = book;
        this.quantity = quantity;
        this.price = price;
    }

    public Sales getSale() {
        return sale;
    }

    public void setSale(Sales sale) {
        this.sale = sale;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
