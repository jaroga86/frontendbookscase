package com.jrg.front.bookscase.dto;

public class AuthorDto {

    private String name;
    private String surnames;

    public AuthorDto(String name, String surnames) {
        this.name = name;
        this.surnames = surnames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }
}
