package com.jrg.front.bookscase.entity;

import com.jrg.front.bookscase.entity.segurity.entity.User;

import java.io.Serializable;

public class Sales implements Serializable{


    private int id;
    private String salesDate;
    private User user;

    public Sales() {
    }

    public Sales(int id, String salesDate, User user) {
        this.id = id;
        this.salesDate = salesDate;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(String salesDate) {
        this.salesDate = salesDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
