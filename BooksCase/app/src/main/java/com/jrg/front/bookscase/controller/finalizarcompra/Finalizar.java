package com.jrg.front.bookscase.controller.finalizarcompra;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentFinalizarBinding;
import com.jrg.front.bookscase.util.SharedPrefManager;


public class Finalizar extends Fragment {

    private FragmentFinalizarBinding binding;

    private SharedPrefManager sharedPrefManager;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                limpiarCarrito();
                NavHostFragment.findNavController(Finalizar.this)
                        .navigate(R.id.action_finalizar_to_mobile_navigation);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);

        // The callback can be enabled or disabled here or in handleOnBackPressed()
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentFinalizarBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        requireActivity().invalidateOptionsMenu();

        ImageView imageView = binding.imageBye;
        Bundle bundle = getArguments();

        if(bundle != null){

            int idSales = bundle.getInt("salesId");

            binding.idNumPedido.setText("Su número de pedido es: " + idSales);

        }


        Glide.with(root).load("https://media.giphy.com/media/igsUFrp3ac8M3HyRh2/giphy.gif").into(imageView);


        binding.idgotohome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                limpiarCarrito();
                NavHostFragment.findNavController(Finalizar.this)
                        .navigate(R.id.action_finalizar_to_mobile_navigation);
            }
        });


        return root;
    }

    protected void limpiarCarrito() {

        sharedPrefManager = new SharedPrefManager(getContext().getApplicationContext());

        sharedPrefManager.cleanCart();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}