package com.jrg.front.bookscase.controller.book;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.adapter.BookAdapter;
import com.jrg.front.bookscase.databinding.FragmentAllBookBinding;
import com.jrg.front.bookscase.entity.Book;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.entity.segurity.enums.RolName;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllBook extends Fragment {

    private List<Book> listaBook = new ArrayList<>();
    private FragmentAllBookBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;

    public AllBook() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAllBookBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Bundle bundle = getArguments();

        sharedPrefManager = new SharedPrefManager(root.getContext());

        if(bundle != null){
            listaBook = (List<Book>) bundle.getSerializable("books");
            binding.idAllBook.setAdapter(new BookAdapter(root.getContext(), R.layout.rowlayout, listaBook));
        }else{
            apiService = Apis.getClient();
            Call<List<Book>> call = apiService.getBooks();

            call.enqueue(new Callback<List<Book>>() {
                @Override
                public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
                    listaBook = response.body();
                    binding.idAllBook.setAdapter(new BookAdapter(root.getContext(), R.layout.rowlayout, listaBook));
                }

                @Override
                public void onFailure(Call<List<Book>> call, Throwable t) {
                    Log.e("Error:", t.getMessage());
                }
            });


        }


        binding.idAllBook.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Bundle args = new Bundle();
                args.putString("isbn", (String) String.valueOf(listaBook.get(i).getIsbn()));

                NavHostFragment.findNavController(AllBook.this)
                        .navigate(R.id.action_allBook_to_desc_book, args);
            }
        });


        if(sharedPrefManager.getUser()!=null){
            if(Utils.hasRol(sharedPrefManager.getUser().getUser(), RolName.ROLE_ADMIN)){
                binding.addBook.setVisibility(Button.VISIBLE);
            }else{
                binding.addBook.setVisibility(Button.INVISIBLE);
            }
        }

        binding.addBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NavHostFragment.findNavController(AllBook.this)
                        .navigate(R.id.action_allBook_to_newBook);
            }
        });


        return root;
    }
}