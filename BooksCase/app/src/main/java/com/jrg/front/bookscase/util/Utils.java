package com.jrg.front.bookscase.util;

import android.content.Context;
import android.util.Log;

import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.dto.JwtDto;
import com.jrg.front.bookscase.entity.segurity.entity.Rol;
import com.jrg.front.bookscase.entity.segurity.entity.User;
import com.jrg.front.bookscase.entity.segurity.enums.RolName;

import java.util.Iterator;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Utils {


    public static String upperCaseFirst(String val) {
        char[] arr = val.toCharArray();
        arr[0] = Character.toUpperCase(arr[0]);
        return new String(arr);
    }


    public static boolean hasRol(User user, RolName roleAdmin) {

        Set<Rol> roles = user.getRoles();

        Iterator<Rol> it = roles.iterator();

        while(it.hasNext()){

            if(it.next().getRolName().name().equalsIgnoreCase(roleAdmin.name())){
                return true;
            }
        }

        return false;

    }

    public static void refreshToken(Context context) {

        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);

        JwtDto jwtDto = sharedPrefManager.getUser();

        APIService apiService = Apis.getClient();


        Call<JwtDto> call = apiService.refreshToken(jwtDto);

        call.enqueue(new Callback<JwtDto>() {
            @Override
            public void onResponse(Call<JwtDto> call, Response<JwtDto> response) {

                jwtDto.setToken(response.body().getToken());

                Log.e("Refresh", "Token creado: " + jwtDto.getToken());

                sharedPrefManager.saveUser(jwtDto);


            }

            @Override
            public void onFailure(Call<JwtDto> call, Throwable t) {

                Log.e("Refresh", "Error al refrescar el token");

            }
        });



    }

}
