package com.jrg.front.bookscase.dto;

import java.util.HashSet;
import java.util.Set;

public class RegisterDto {

    private String name;
    private String surnames;
    private String password;
    private String email;
    private String address;
    private int cp;
    private String location;
    private String province;

    private boolean state;
    private Set<String> roles = new HashSet<>();

    public RegisterDto(String name, String surnames, String password, String email, String address, int cp, String location, String province, boolean state, Set<String> roles) {
        this.name = name;
        this.surnames = surnames;
        this.password = password;
        this.email = email;
        this.address = address;
        this.cp = cp;
        this.location = location;
        this.province = province;
        this.state = state;
        this.roles = roles;
    }

    public RegisterDto(String name, String surnames, String password, String email, String address, int cp, String location, String province, boolean state) {
        this.name = name;
        this.surnames = surnames;
        this.password = password;
        this.email = email;
        this.address = address;
        this.cp = cp;
        this.location = location;
        this.province = province;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
