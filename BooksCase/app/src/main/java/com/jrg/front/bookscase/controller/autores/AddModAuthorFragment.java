package com.jrg.front.bookscase.controller.autores;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentAddModAuthorBinding;
import com.jrg.front.bookscase.entity.Author;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.dto.AuthorDto;
import com.jrg.front.bookscase.dto.Mensaje;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddModAuthorFragment extends Fragment {

    private FragmentAddModAuthorBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private Author author;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAddModAuthorBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());
        apiService = Apis.getClient();

        Bundle bundle = getArguments();

        if(bundle != null){
            author = (Author) bundle.getSerializable("author");
            binding.editAuthorName.setText(author.getName());
            binding.editAuthorSurnames.setText(author.getSurnames());
            binding.idSaveAuthor.setVisibility(Button.INVISIBLE);
        }else{

            binding.idModificarAuthor.setVisibility(Button.INVISIBLE);
            binding.idEliminarAuthor.setVisibility(Button.INVISIBLE);
        }




        binding.idSaveAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Call<Mensaje> call = apiService.postAuthor(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                        new AuthorDto(binding.editAuthorName.getText().toString(), binding.editAuthorSurnames.getText().toString()));
                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {

                        responseClient(response);


                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });
            }
        });


        binding.idModificarAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Call<Mensaje> call = apiService.updateAuthor(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), author.getId(),
                        new AuthorDto(binding.editAuthorName.getText().toString(), binding.editAuthorSurnames.getText().toString()));

                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {

                        responseClient(response);



                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });
            }
        });


        binding.idEliminarAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Call<Mensaje> call = apiService.deleteAuthor(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), author.getId());

                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {

                        responseClient(response);

                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });


            }
        });


        return root;
    }


    protected void responseClient(Response<Mensaje> response){

        Mensaje mensaje;

        if(!response.isSuccessful() && response.code() == 400){
            try{
                Gson gson = new Gson();
                String jsonError = response.errorBody().string();
                mensaje = gson.fromJson(jsonError, Mensaje.class);
                Toast.makeText(getContext().getApplicationContext(),  Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();
            }catch (IOException io){
                io.printStackTrace();
            }

        }else if(response.code() == 200){

            mensaje = response.body();


            Toast.makeText(getContext().getApplicationContext(), Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();

            NavHostFragment.findNavController(AddModAuthorFragment.this)
                    .navigate(R.id.action_addModAuthor_to_nav_author);
        }
    }
}