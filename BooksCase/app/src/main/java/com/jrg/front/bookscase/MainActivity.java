package com.jrg.front.bookscase;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.jrg.front.bookscase.databinding.ActivityMainBinding;
import com.jrg.front.bookscase.entity.segurity.enums.RolName;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;
    private SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);
      /*  binding.appBarMain.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_book, R.id.nav_author, R.id.nav_category, R.id.nav_language, R.id.nav_sales)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        sharedPrefManager = new SharedPrefManager(getApplicationContext());


        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if(destination.getId() == R.id.finalizar){
                    binding.appBarMain.toolbar.setVisibility(View.GONE);
                }else{
                    binding.appBarMain.toolbar.setVisibility(View.VISIBLE);
                }
            }
        });





    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_logout:

                sharedPrefManager.logout();

                TextView nameUser = (TextView) binding.navView.getHeaderView(0).findViewById(R.id.idNameUser);
                nameUser.setVisibility(TextView.INVISIBLE);


                TextView emailUser = (TextView) binding.navView.getHeaderView(0).findViewById(R.id.idEmailUser);
                emailUser.setVisibility(TextView.INVISIBLE);

                item.setVisible(false);

                invalidateOptionsMenu();


               // NavController navController2 = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);

                NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);


                navController.navigate(R.id.to_home);

                return  super.onOptionsItemSelected(item);

            case R.id.nav_login:
                NavController navController1 = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);

                return NavigationUI.onNavDestinationSelected(item, navController1)
                        || super.onOptionsItemSelected(item);
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        if(sharedPrefManager.isLoggedIn()){
            TextView nameUser = (TextView) binding.navView.getHeaderView(0).findViewById(R.id.idNameUser);
            nameUser.setVisibility(TextView.VISIBLE);
            nameUser.setText(sharedPrefManager.getUser().getUser().getName() + " " + sharedPrefManager.getUser().getUser().getSurnames());

            TextView emailUser = (TextView) binding.navView.getHeaderView(0).findViewById(R.id.idEmailUser);
            emailUser.setVisibility(TextView.VISIBLE);
            emailUser.setText(sharedPrefManager.getUser().getUser().getEmail());

            MenuItem nav_login = (MenuItem) menu.findItem(R.id.nav_login);
            nav_login.setVisible(false);
            MenuItem nav_logout = (MenuItem) menu.findItem(R.id.nav_logout);
            nav_logout.setVisible(true);

            if(Utils.hasRol(sharedPrefManager.getUser().getUser(), RolName.ROLE_ADMIN)){
                Log.d("ROL", "Es admin");

               MenuItem nav_book = binding.navView.getMenu().findItem(R.id.nav_book);
               nav_book.setVisible(true);

               MenuItem nav_author = binding.navView.getMenu().findItem(R.id.nav_author);
               nav_author.setVisible(true);

               MenuItem nav_category = binding.navView.getMenu().findItem(R.id.nav_category);
               nav_category.setVisible(true);

               MenuItem nav_language = binding.navView.getMenu().findItem(R.id.nav_language);
               nav_language.setVisible(true);

               MenuItem nav_ventas = binding.navView.getMenu().findItem(R.id.nav_sales);
               nav_ventas.setVisible(true);

                Utils.refreshToken(getApplicationContext());


            }else{
                Log.d("ROL", "Es cliente");
            }

        }else{

            TextView nameUser = (TextView) binding.navView.getHeaderView(0).findViewById(R.id.idNameUser);
            nameUser.setVisibility(TextView.INVISIBLE);

            TextView emailUser = (TextView) binding.navView.getHeaderView(0).findViewById(R.id.idEmailUser);
            emailUser.setVisibility(TextView.INVISIBLE);

            MenuItem nav_login = (MenuItem) menu.findItem(R.id.nav_login);
            nav_login.setVisible(true);
            MenuItem nav_logout = (MenuItem) menu.findItem(R.id.nav_logout);
            nav_logout.setVisible(false);


            MenuItem nav_book = binding.navView.getMenu().findItem(R.id.nav_book);
            nav_book.setVisible(false);

            MenuItem nav_author = binding.navView.getMenu().findItem(R.id.nav_author);
            nav_author.setVisible(false);

            MenuItem nav_category = binding.navView.getMenu().findItem(R.id.nav_category);
            nav_category.setVisible(false);

            MenuItem nav_language = binding.navView.getMenu().findItem(R.id.nav_language);
            nav_language.setVisible(false);

            MenuItem nav_ventas = binding.navView.getMenu().findItem(R.id.nav_sales);
            nav_ventas.setVisible(false);

        }


        return true;
    }



    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}