package com.jrg.front.bookscase.entity.segurity.entity;

import com.jrg.front.bookscase.entity.segurity.enums.RolName;

public class Rol {

    private int id;
    private RolName rolName;

    public Rol(int id, RolName rolName) {
        this.id = id;
        this.rolName = rolName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RolName getRolName() {
        return rolName;
    }

    public void setRolName(RolName rolName) {
        this.rolName = rolName;
    }
}
