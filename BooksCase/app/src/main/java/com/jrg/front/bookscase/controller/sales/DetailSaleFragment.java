package com.jrg.front.bookscase.controller.sales;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.adapter.DetailSaleAdapter;
import com.jrg.front.bookscase.databinding.FragmentDetailSaleBinding;
import com.jrg.front.bookscase.entity.DetailSale;
import com.jrg.front.bookscase.entity.Sales;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailSaleFragment extends Fragment {

    private FragmentDetailSaleBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private List<DetailSale> listaDetalleVenta = new ArrayList<>();
    private Sales sale;
    private static final DecimalFormat df = new DecimalFormat("0.00");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentDetailSaleBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());
        apiService = Apis.getClient();

        Bundle bundle = getArguments();

        if(bundle != null){
            sale = (Sales) bundle.getSerializable("sales");
        }

        binding.valueDescDatosCliente.setText(sale.getUser().getName() + " " + sale.getUser().getSurnames() + "\n"+
        sale.getUser().getAddress() + ", " + sale.getUser().getLocation() + "\n"
                + sale.getUser().getProvince() + ", " + sale.getUser().getCp());

        Call<List<DetailSale>> call = apiService.getBySaleId(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), sale.getId());

        call.enqueue(new Callback<List<DetailSale>>() {
            @Override
            public void onResponse(Call<List<DetailSale>> call, Response<List<DetailSale>> response) {
                listaDetalleVenta = response.body();
                binding.descSaleLv.setAdapter(new DetailSaleAdapter(root.getContext(), R.layout.rowcart, listaDetalleVenta));

                float totalSales = 0;

                for(DetailSale detailSale: listaDetalleVenta ){

                    totalSales += detailSale.getQuantity() * detailSale.getPrice();
                }

                binding.totalSales.setText(String.valueOf(df.format(totalSales)) +"€");

            }

            @Override
            public void onFailure(Call<List<DetailSale>> call, Throwable t) {

            }
        });


        // Inflate the layout for this fragment
        return root;
    }
}