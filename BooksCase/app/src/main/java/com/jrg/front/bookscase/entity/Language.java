package com.jrg.front.bookscase.entity;

import java.io.Serializable;

public class Language implements Serializable {

    private int id;
    private String language;

    public Language(int id, String language) {
        this.id = id;
        this.language = language;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
