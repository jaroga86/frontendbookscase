package com.jrg.front.bookscase.controller.login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentLoginBinding;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.dto.JwtDto;
import com.jrg.front.bookscase.dto.LoginDto;
import com.jrg.front.bookscase.util.Apis;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends Fragment {

   // private LoginViewModel loginViewModel;
    private FragmentLoginBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

      binding = FragmentLoginBinding.inflate(inflater, container, false);
      return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        final EditText usernameEditText = binding.username;
        final EditText passwordEditText = binding.password;
        final Button loginButton = binding.login;
        final Button registerButton = binding.register;


        sharedPrefManager = new SharedPrefManager(getContext().getApplicationContext());


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiService = Apis.getClient();

                Log.d("LoginFragment", "login " +usernameEditText.getText().toString() + " Pass " + passwordEditText.getText().toString());
                Call<JwtDto> call = apiService.postLogin(new LoginDto(usernameEditText.getText().toString(), passwordEditText.getText().toString()));

                call.enqueue(new Callback<JwtDto>() {
                    @Override
                    public void onResponse(Call<JwtDto> call, Response<JwtDto> response) {
                        JwtDto jwtDto = response.body();



                        if(response.code() == 200){

                            sharedPrefManager.saveUser(response.body());

                            Toast.makeText(getContext().getApplicationContext(), "Bienvenido "  + jwtDto.getUser().getName(), Toast.LENGTH_SHORT).show();



                            NavHostFragment.findNavController(LoginFragment.this)
                                    .navigate(R.id.action_nav_login_to_nav_home);

                        }else if(response.code() == 401){
                            Toast.makeText(getContext().getApplicationContext(), "El usuario o la contraseñas es incorrecta.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JwtDto> call, Throwable t) {

                        Log.d("LoginFragment", "Algo falla en el login " + t.getMessage());

                    }
                });



            }
        });


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(LoginFragment.this)
                        .navigate(R.id.action_nav_login_to_register2);
            }
        });
    }



@Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}