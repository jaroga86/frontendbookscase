package com.jrg.front.bookscase.interfaces;

import com.jrg.front.bookscase.entity.Author;
import com.jrg.front.bookscase.entity.Book;
import com.jrg.front.bookscase.entity.Category;
import com.jrg.front.bookscase.entity.DetailSale;
import com.jrg.front.bookscase.entity.Language;
import com.jrg.front.bookscase.entity.Sales;
import com.jrg.front.bookscase.dto.AuthorDto;
import com.jrg.front.bookscase.dto.BookDto;
import com.jrg.front.bookscase.dto.CategoryDto;
import com.jrg.front.bookscase.dto.DetailSaleDto;
import com.jrg.front.bookscase.dto.JwtDto;
import com.jrg.front.bookscase.dto.LanguageDto;
import com.jrg.front.bookscase.dto.LoginDto;
import com.jrg.front.bookscase.dto.Mensaje;
import com.jrg.front.bookscase.dto.RegisterDto;
import com.jrg.front.bookscase.dto.SalesDto;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;

import retrofit2.http.DELETE;
import retrofit2.http.GET;
;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;


public interface APIService {

    @Headers("Content-Type: application/json")
    @GET("book/lista")
    public Call<List<Book>> getBooks();

    @Headers("Content-Type: application/json")
    @GET("book/detail/{isbn}")
    public Call<Book> getBook(@Path("isbn") long isbn);


    @Headers("Content-Type: application/json")
    @POST("auth/login")
    public Call<JwtDto> postLogin(@Body LoginDto userLogin);


    @Headers("Content-Type: application/json")
    @POST("auth/nuevo")
    public Call<Mensaje> postRegister(@Body RegisterDto registerDto);
    @Headers("Content-Type: application/json")
    @GET("author/lista")
    public Call<List<Author>> getAuthors(@Header("Authorization") String authHeader);

    @Headers("Content-Type: application/json")
    @GET("category/lista")
    public Call<List<Category>> getCategories(@Header("Authorization") String authHeader);

    @Headers("Content-Type: application/json")
    @GET("language/lista")
    public Call<List<Language>> getLanguages(@Header("Authorization") String authHeader);


    @Headers("Content-Type: application/json")
    @GET("sales/lista")
    public Call<List<Sales>> getSales(@Header("Authorization") String authHeader);

    @Headers("Content-Type: application/json")
    @GET("detailsale/bySale/{idSale}")
    public Call<List<DetailSale>> getBySaleId(@Header("Authorization") String authHeader, @Path("idSale") int idSale);


    @Headers("Content-Type: application/json")
    @POST("auth/refresh")
    public Call<JwtDto> refreshToken(@Body JwtDto jwtDto);

    //Crear la Venta
    @Headers("Content-Type: application/json")
    @POST("sales/create")
    public Call<Sales> postSales(@Header("Authorization") String authHeader, @Body SalesDto salesDto);

    //Crear detalle de la venta
    @Headers("Content-Type: application/json")
    @POST("detailsale/create")
    public Call<Mensaje> postDetailSale(@Header("Authorization") String authHeader, @Body DetailSaleDto detailSaleDto);

    //Crear Autores
    @Headers("Content-Type: application/json")
    @POST("author/create")
    public Call<Mensaje> postAuthor(@Header("Authorization") String authHeader, @Body AuthorDto authorDto);


    //Update Autor
    @Headers("Content-Type: application/json")
    @PUT("author/update/{id}")
    public Call<Mensaje> updateAuthor(@Header("Authorization") String authHeader, @Path("id") int id, @Body AuthorDto authorDto);

    //Eliminar Autor
    @Headers("Content-Type: application/json")
    @DELETE("author/delete/{id}")
    public Call<Mensaje> deleteAuthor(@Header("Authorization") String authHeader, @Path("id") int id);


    //Crear Categorias
    @Headers("Content-Type: application/json")
    @POST("category/create")
    public Call<Mensaje> postCategory(@Header("Authorization") String authHeader, @Body CategoryDto categoryrDto);


    //Update Categorias
    @Headers("Content-Type: application/json")
    @PUT("category/update/{id}")
    public Call<Mensaje> updateCategory(@Header("Authorization") String authHeader, @Path("id") int id, @Body CategoryDto categoryrDto);

    //Eliminar Categorias
    @Headers("Content-Type: application/json")
    @DELETE("category/delete/{id}")
    public Call<Mensaje> deleteCategory(@Header("Authorization") String authHeader, @Path("id") int id);

    //Crear Lenguaje
    @Headers("Content-Type: application/json")
    @POST("language/create")
    public Call<Mensaje> postLanguage(@Header("Authorization") String authHeader, @Body LanguageDto languageDto);


    //Update Lenguaje
    @Headers("Content-Type: application/json")
    @PUT("language/update/{id}")
    public Call<Mensaje> updateLanguage(@Header("Authorization") String authHeader, @Path("id") int id, @Body LanguageDto languageDto);

    //Eliminar Lenguaje
    @Headers("Content-Type: application/json")
    @DELETE("language/delete/{id}")
    public Call<Mensaje> deleteLanguage(@Header("Authorization") String authHeader, @Path("id") int id);

    //Obtiene el lenguaje por el nombre
    @Headers("Content-Type: application/json")
    @GET("language/detaillanguage/{language}")
    public Call<Language> getLanguage(@Header("Authorization") String authHeader, @Path("language") String language);


    //Obtiene la categoria por el nombre
    @Headers("Content-Type: application/json")
    @GET("category/detailname/{category}")
    public Call<Category> getCategory(@Header("Authorization") String authHeader, @Path("category") String category);

    //Obtiene author por el nombre
    @Headers("Content-Type: application/json")
    @GET("author/detailnamesurnames/{name}/{surnames}")
    public Call<Author> getAuthor(@Header("Authorization") String authHeader, @Path("name") String name , @Path("surnames") String surnames);


    //Subir Imagen
    @Multipart
    @POST("file/uploadFile")
    public Call<Mensaje> uploadImage(@Header("Authorization") String authHeader, @Part MultipartBody.Part image);

    //Crear libro
    @Headers("Content-Type: application/json")
    @POST("book/create")
    public Call<Mensaje> postBook(@Header("Authorization") String authHeader, @Body BookDto bookDto);

    //Update libro
    @Headers("Content-Type: application/json")
    @PUT("book/update/{isbn}")
    public Call<Mensaje> updateBook(@Header("Authorization") String authHeader, @Path("isbn") long isbn, @Body BookDto bookDto);

    //Eliminar book
    @Headers("Content-Type: application/json")
    @DELETE("book/delete/{isbn}")
    public Call<Mensaje> deleteBook(@Header("Authorization") String authHeader, @Path("isbn") long isbn);


}
