package com.jrg.front.bookscase.controller.categories;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.adapter.CategoryAdapter;
import com.jrg.front.bookscase.databinding.FragmentCategoryBinding;
import com.jrg.front.bookscase.entity.Category;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.entity.segurity.enums.RolName;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends Fragment {

    private FragmentCategoryBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private List<Category> listaCategories = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentCategoryBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());

        apiService = Apis.getClient();

        Call<List<Category>> call = apiService.getCategories(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken());

        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                listaCategories = response.body();
                binding.idAllCategory.setAdapter(new CategoryAdapter(root.getContext(), R.layout.rowauthor, listaCategories));
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

                Log.e("Category:", t.getMessage());

            }
        });

        binding.idAllCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Bundle args = new Bundle();

                args.putSerializable("category", listaCategories.get(i));

                NavHostFragment.findNavController(CategoryFragment.this)
                        .navigate(R.id.action_nav_category_to_addModCategoryFragment, args);
            }
        });

        if(sharedPrefManager.getUser()!=null){
            if(Utils.hasRol(sharedPrefManager.getUser().getUser(), RolName.ROLE_ADMIN)){
                binding.addCategory.setVisibility(Button.VISIBLE);
            }else{
                binding.addCategory.setVisibility(Button.INVISIBLE);
            }
        }

        binding.addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(CategoryFragment.this)
                        .navigate(R.id.action_nav_category_to_addModCategoryFragment);
            }
        });

        // Inflate the layout for this fragment
        return root;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}