package com.jrg.front.bookscase.entity;

import java.io.Serializable;

public class DetailSale implements Serializable {


    private int id;
    private Sales sale;
    private Book book;
    private int quantity;
    private float price;

    public DetailSale() {
    }

    public DetailSale(int id, Sales sale, Book book, int quantity, float price) {
        this.id = id;
        this.sale = sale;
        this.book = book;
        this.quantity = quantity;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Sales getSale() {
        return sale;
    }

    public void setSale(Sales sale) {
        this.sale = sale;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
