package com.jrg.front.bookscase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.entity.DetailSale;
import com.jrg.front.bookscase.util.Apis;

import java.text.DecimalFormat;
import java.util.List;

public class DetailSaleAdapter extends ArrayAdapter<DetailSale> {

    private Context context;
    private List<DetailSale> detailSales;
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public DetailSaleAdapter(@NonNull Context context, int resource, @NonNull List<DetailSale> objects) {
        super(context, resource, objects);
        this.context = context;
        this.detailSales = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = layoutInflater.inflate(R.layout.rowcart, parent, false);

        ImageView cartBookPortada = (ImageView) rowView.findViewById(R.id.idCartBookPortada);
        TextView cartBookAuthor = (TextView) rowView.findViewById(R.id.idCartBookAuthor);
        TextView cartBookName = (TextView) rowView.findViewById(R.id.idCartBookName);
        EditText cartDescQuantybook = (EditText) rowView.findViewById(R.id.idCartDescQuantybook);

        TextView cartBookPrice = (TextView) rowView.findViewById(R.id.idCartBookPrice);
        TextView cartBookSubPrice = (TextView) rowView.findViewById(R.id.idCartBookSubPrice);



        Glide.with(rowView).load(Apis.getUrlImg(detailSales.get(position).getBook().getPath_img())).into(cartBookPortada);
        cartBookAuthor.setText(detailSales.get(position).getBook().getAuthors().iterator().next().getName() + " " + detailSales.get(position).getBook().getAuthors().iterator().next().getSurnames());
        cartBookName.setText(detailSales.get(position).getBook().getName());
        cartDescQuantybook.setText(String.valueOf(detailSales.get(position).getQuantity()));

        cartBookPrice.setText(String.valueOf(df.format(detailSales.get(position).getBook().getPrice())+"€"));

        float subPrice = detailSales.get(position).getBook().getPrice() * detailSales.get(position).getQuantity();
        cartBookSubPrice.setText(String.valueOf(df.format(subPrice))+"€");

        return rowView;
    }
}
