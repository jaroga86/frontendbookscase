package com.jrg.front.bookscase.controller.autores;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;

import androidx.navigation.fragment.NavHostFragment;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.adapter.AuthorAdapter;
import com.jrg.front.bookscase.databinding.FragmentAutoresBinding;
import com.jrg.front.bookscase.entity.Author;

import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.entity.segurity.enums.RolName;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AuthorFragment extends Fragment {


    private FragmentAutoresBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private List<Author> listaAuthor = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentAutoresBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());


        apiService = Apis.getClient();


        Call<List<Author>> call = apiService.getAuthors(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken());

        call.enqueue(new Callback<List<Author>>() {
            @Override
            public void onResponse(Call<List<Author>> call, Response<List<Author>> response) {
                listaAuthor = response.body();
                binding.idAllAuthor.setAdapter(new AuthorAdapter(root.getContext(), R.layout.rowauthor, listaAuthor));
            }

            @Override
            public void onFailure(Call<List<Author>> call, Throwable t) {

                Log.e("Lista autores:", t.getMessage());

            }
        });

        binding.idAllAuthor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Bundle args = new Bundle();

                args.putSerializable("author", listaAuthor.get(i));

                NavHostFragment.findNavController(AuthorFragment.this)
                        .navigate(R.id.action_nav_author_to_addModAuthor, args);

            }
        });


        if(sharedPrefManager.getUser()!=null){
            if(Utils.hasRol(sharedPrefManager.getUser().getUser(), RolName.ROLE_ADMIN)){
                binding.addAuthor.setVisibility(Button.VISIBLE);
            }else{
                binding.addAuthor.setVisibility(Button.INVISIBLE);
            }
        }

        binding.addAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NavHostFragment.findNavController(AuthorFragment.this)
                        .navigate(R.id.action_nav_author_to_addModAuthor);

            }
        });


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}