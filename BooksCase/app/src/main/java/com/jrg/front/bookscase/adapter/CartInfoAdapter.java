package com.jrg.front.bookscase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.entity.CartLineInfo;
import com.jrg.front.bookscase.util.Apis;

import java.text.DecimalFormat;
import java.util.List;

public class CartInfoAdapter extends ArrayAdapter<CartLineInfo> {

    private Context context;
    private List<CartLineInfo> cartLineInfos;
    private static final DecimalFormat df = new DecimalFormat("0.00");


    public CartInfoAdapter(@NonNull Context context, int resource, @NonNull List<CartLineInfo> cartLineInfos) {
        super(context, resource, cartLineInfos);
        this.context = context;
        this.cartLineInfos = cartLineInfos;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = layoutInflater.inflate(R.layout.rowcart, parent, false);

        ImageView cartBookPortada = (ImageView) rowView.findViewById(R.id.idCartBookPortada);
        TextView cartBookAuthor = (TextView) rowView.findViewById(R.id.idCartBookAuthor);
        TextView cartBookName = (TextView) rowView.findViewById(R.id.idCartBookName);
        EditText cartDescQuantybook = (EditText) rowView.findViewById(R.id.idCartDescQuantybook);

        TextView cartBookPrice = (TextView) rowView.findViewById(R.id.idCartBookPrice);
        TextView cartBookSubPrice = (TextView) rowView.findViewById(R.id.idCartBookSubPrice);



        Glide.with(rowView).load(Apis.getUrlImg(cartLineInfos.get(position).getBook().getPath_img())).into(cartBookPortada);
        cartBookAuthor.setText(cartLineInfos.get(position).getBook().getAuthors().iterator().next().getName() + " " + cartLineInfos.get(position).getBook().getAuthors().iterator().next().getSurnames());
        cartBookName.setText(cartLineInfos.get(position).getBook().getName());
        cartDescQuantybook.setText(String.valueOf(cartLineInfos.get(position).getQuantity()));

        cartBookPrice.setText(String.valueOf(df.format(cartLineInfos.get(position).getBook().getPrice())+"€"));

        float subPrice = cartLineInfos.get(position).getBook().getPrice() * cartLineInfos.get(position).getQuantity();
        cartBookSubPrice.setText(String.valueOf(df.format(subPrice))+"€");




        return rowView;
    }
}
