package com.jrg.front.bookscase.util;

import android.util.Log;

import com.jrg.front.bookscase.interfaces.APIService;

public class Apis {

    public static final String URL = "http://192.168.1.189:8080/";

    public static APIService getClient(){
        Log.d( "Apis", "Paso por aqui el valor de la url es: " + URL);
        return Client.getClient(URL).create(APIService.class);
    }

    public static String getUrlImg(String nameFile){
       return URL + "file/portadas/" + nameFile;
    }
}
