package com.jrg.front.bookscase.controller.sales;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.adapter.SalesAdapter;
import com.jrg.front.bookscase.databinding.FragmentSalesBinding;
import com.jrg.front.bookscase.entity.Sales;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SalesFragment extends Fragment {

    private FragmentSalesBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private List<Sales> listaSales = new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSalesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());

        apiService = Apis.getClient();

        Call<List<Sales>> call = apiService.getSales(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken());

        call.enqueue(new Callback<List<Sales>>() {
            @Override
            public void onResponse(Call<List<Sales>> call, Response<List<Sales>> response) {
                listaSales = response.body();
                binding.idAllSales.setAdapter(new SalesAdapter(root.getContext(), R.layout.rowsales, listaSales));



            }

            @Override
            public void onFailure(Call<List<Sales>> call, Throwable t) {
                Log.e("Sales:", t.getMessage());
            }
        });

        binding.idAllSales.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Bundle args = new Bundle();

                args.putSerializable("sales", listaSales.get(i));

                NavHostFragment.findNavController(SalesFragment.this)
                        .navigate(R.id.action_nav_sales_to_detailSaleFragment, args);


            }
        });



        return root;
    }
}