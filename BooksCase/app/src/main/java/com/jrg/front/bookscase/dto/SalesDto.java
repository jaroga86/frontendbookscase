package com.jrg.front.bookscase.dto;

import com.jrg.front.bookscase.entity.segurity.entity.User;



public class SalesDto {


    private String salesDate;
    private User user;

    public SalesDto(String salesDate, User user) {
        this.salesDate = salesDate;
        this.user = user;
    }

    public String getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(String salesDate) {
        this.salesDate = salesDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
