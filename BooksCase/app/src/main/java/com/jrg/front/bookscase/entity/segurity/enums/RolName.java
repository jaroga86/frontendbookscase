package com.jrg.front.bookscase.entity.segurity.enums;

public enum RolName {
    ROLE_ADMIN, ROLE_USER
}
