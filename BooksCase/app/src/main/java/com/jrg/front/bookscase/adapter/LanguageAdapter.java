package com.jrg.front.bookscase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.entity.Category;
import com.jrg.front.bookscase.entity.Language;

import java.util.List;

public class LanguageAdapter extends ArrayAdapter<Language> {

    private Context context;
    private List<Language> languages;

    public LanguageAdapter(@NonNull Context context, int resource, @NonNull List<Language> objects) {
        super(context, resource, objects);
        this.context = context;
        this.languages = objects;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = layoutInflater.inflate(R.layout.rowlanguage, parent, false);

        TextView nameLanguage = (TextView) rowView.findViewById(R.id.nameLanguage);

        nameLanguage.setText(languages.get(position).getLanguage());


        return rowView;
    }
}
