package com.jrg.front.bookscase.controller.register;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentRegisterBinding;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.dto.Mensaje;
import com.jrg.front.bookscase.dto.RegisterDto;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterFragment extends Fragment {


    private FragmentRegisterBinding binding;
    private APIService apiService;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = FragmentRegisterBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final Button idSave = binding.idSaveUser;

        idSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(binding.editPassword.getText().toString().equalsIgnoreCase(binding.editRePassword.getText().toString())){


                apiService = Apis.getClient();
                Call<Mensaje> call = apiService.postRegister(new RegisterDto(binding.editNombre.getText().toString(),binding.editApellidos.getText().toString(),
                        binding.editPassword.getText().toString(), binding.editEmail.getText().toString(), binding.editDireccion.getText().toString(),
                        Integer.valueOf((binding.editCp.getText().toString().equals(""))?"0":binding.editCp.getText().toString()), binding.editCiudad.getText().toString(), binding.editProvincia.getText().toString(), true));

                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {

                        Mensaje mensaje;

                        Log.d("Register" ,response.toString());

                        if(!response.isSuccessful() && response.code() == 400){
                              try{
                                  Gson gson = new Gson();
                                  String jsonError = response.errorBody().string();
                                  mensaje = gson.fromJson(jsonError, Mensaje.class);
                                  Toast.makeText(getContext().getApplicationContext(),  Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();
                              }catch (IOException io){
                                    io.printStackTrace();
                              }

                        }else if(response.code() == 201){

                            mensaje = response.body();


                            Toast.makeText(getContext().getApplicationContext(), Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();

                            NavHostFragment.findNavController(RegisterFragment.this)
                                    .navigate(R.id.action_register2_to_nav_login);
                        }else if(response.code() == 401){
                            Toast.makeText(getContext().getApplicationContext(), "Se deben rellenar todos los campos correctamente.", Toast.LENGTH_SHORT).show();
                        }



                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                        Log.d("RegisterFragment", "Algo falla en el registro " + t.getMessage());

                    }
                });


                }else{
                    Toast.makeText(getContext().getApplicationContext(), "La password no coincide", Toast.LENGTH_LONG).show();
                }


            }
        });


        return root;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}