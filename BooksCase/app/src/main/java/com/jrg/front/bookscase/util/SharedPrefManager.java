package com.jrg.front.bookscase.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.jrg.front.bookscase.entity.CartInfo;
import com.jrg.front.bookscase.dto.JwtDto;

public class SharedPrefManager {

    private static String SHARED_PREF_NAME = "bookscase";
    private SharedPreferences sharedPreferences;
    Context context;
    private SharedPreferences.Editor editor;

    public SharedPrefManager(Context context) {
        this.context = context;
    }


    public void saveUser(JwtDto jwtDto){

        Gson gson = new Gson();
        String json = gson.toJson(jwtDto);
        sharedPreferences=context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor.putString("json", json);
        editor.putBoolean("logged", true);
        editor.apply();
    }

    public boolean isLoggedIn(){
        sharedPreferences=context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("logged", false);
    }

    public JwtDto getUser(){
        sharedPreferences=context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("json", "");
        JwtDto jwtDto = gson.fromJson(json, JwtDto.class);
        return jwtDto;
    }

    public void saveCart(CartInfo cartInfo){
        Gson gson = new Gson();
        String json = gson.toJson(cartInfo);
        sharedPreferences=context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor.putString("cart", json);
        editor.apply();
    }

    public CartInfo getCartInfo(){
        sharedPreferences=context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("cart", "");
        CartInfo cartInfo = gson.fromJson(json, CartInfo.class);
        return cartInfo;
    }

    public void cleanCart(){
        sharedPreferences=context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor.remove("cart");
        editor.apply();
    }

    public void logout(){
        sharedPreferences=context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }



}
