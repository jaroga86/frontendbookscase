package com.jrg.front.bookscase.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;


import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.entity.Sales;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SalesAdapter extends ArrayAdapter<Sales> {

    private Context context;
    private List<Sales> sales;

    public SalesAdapter(@NonNull Context context, int resource, @NonNull List<Sales> objects) {
        super(context, resource, objects);
        this.context = context;
        this.sales = objects;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = layoutInflater.inflate(R.layout.rowsales, parent, false);
        TextView valueId = (TextView) rowView.findViewById(R.id.valueIdVenta);
        TextView valueDate = (TextView) rowView.findViewById(R.id.valueDateVenta);
        TextView valDatosClient = (TextView)  rowView.findViewById(R.id.valueDatosClient);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.ENGLISH);
        LocalDateTime dateTime = LocalDateTime.parse(sales.get(position).getSalesDate(), formatter);

        valueId.setText("" + sales.get(position).getId());
        valueDate.setText("" + dateTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")));
        valDatosClient.setText("" + sales.get(position).getUser().getName() + " " + sales.get(position).getUser().getSurnames() + "\n" + sales.get(position).getUser().getEmail());




        return rowView;
    }
}
