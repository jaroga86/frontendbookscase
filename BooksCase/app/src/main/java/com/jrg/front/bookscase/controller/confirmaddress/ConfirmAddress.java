package com.jrg.front.bookscase.controller.confirmaddress;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentConfirmAddressBinding;
import com.jrg.front.bookscase.entity.DetailSale;
import com.jrg.front.bookscase.entity.Sales;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.entity.CartInfo;
import com.jrg.front.bookscase.entity.CartLineInfo;
import com.jrg.front.bookscase.dto.DetailSaleDto;
import com.jrg.front.bookscase.dto.Mensaje;
import com.jrg.front.bookscase.dto.SalesDto;
import com.jrg.front.bookscase.entity.segurity.entity.User;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConfirmAddress extends Fragment {

    private FragmentConfirmAddressBinding binding;
    private SharedPrefManager sharedPrefManager;
    private User user;
    private APIService apiService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentConfirmAddressBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        sharedPrefManager = new SharedPrefManager(getContext().getApplicationContext());

        User user = sharedPrefManager.getCartInfo().getUser();

        binding.edName.setText(user.getName());
        binding.edSurnames.setText(user.getSurnames());
        binding.edAddress.setText(user.getAddress());
        binding.edLocation.setText(user.getLocation());
        binding.edProvince.setText(user.getProvince());
        binding.edCp.setText(String.valueOf(user.getCp()));

        binding.idFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user.setName(binding.edName.getText().toString());
                user.setSurnames(binding.edSurnames.getText().toString());
                user.setAddress(binding.edAddress.getText().toString());
                user.setLocation(binding.edLocation.getText().toString());
                user.setProvince(binding.edProvince.getText().toString());
                user.setCp(Integer.valueOf(binding.edCp.getText().toString()));

                CartInfo cartInfo = sharedPrefManager.getCartInfo();

                cartInfo.setUser(user);


                sharedPrefManager.saveCart(cartInfo);
                saveSale(cartInfo);


            }

            private void saveSale(CartInfo cartInfo) {

                apiService = Apis.getClient();


                Sales responseSales = new Sales();

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

                String dateString = simpleDateFormat.format(new Date());

                SalesDto salesDto = new SalesDto(dateString, cartInfo.getUser());

                Call<Sales> call = apiService.postSales(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                        salesDto);

                call.enqueue(new Callback<Sales>() {

                    @Override
                    public void onResponse(Call<Sales> call, Response<Sales> response) {
                        responseSales.setSalesDate(response.body().getSalesDate());
                        responseSales.setId(response.body().getId());
                        responseSales.setUser(response.body().getUser());

                        Log.d("ConfirmAddress", "" + responseSales.getSalesDate());

                        for(CartLineInfo cartLineInfo: cartInfo.getCartLines()){
                            DetailSale detailSale = new DetailSale();
                            detailSale.setBook(cartLineInfo.getBook());
                            detailSale.setSale(responseSales);
                            detailSale.setPrice(cartLineInfo.getBook().getPrice());
                            detailSale.setQuantity(cartLineInfo.getQuantity());

                            Call<Mensaje> call1 = apiService.postDetailSale(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                                    new DetailSaleDto(detailSale.getSale(), detailSale.getBook(), detailSale.getQuantity(), detailSale.getPrice()));

                            call1.enqueue(new Callback<Mensaje>() {
                                @Override
                                public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                                    Mensaje mensaje = response.body();
                                    Log.d("ConfirmAddress", mensaje.getMensaje());
                                }

                                @Override
                                public void onFailure(Call<Mensaje> call, Throwable t) {

                                }
                            });

                        }

                        Bundle args = new Bundle();

                        args.putInt("salesId", responseSales.getId());


                        NavHostFragment.findNavController(ConfirmAddress.this)
                                .navigate(R.id.action_confirmAddress_to_finalizar, args);


                    }

                    @Override
                    public void onFailure(Call<Sales> call, Throwable t) {

                    }

                });





            }
        });


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}