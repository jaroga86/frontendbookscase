package com.jrg.front.bookscase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.entity.Category;

import java.util.List;

public class CategoryAdapter extends ArrayAdapter<Category> {


    private Context context;
    private List<Category> categories;



    public CategoryAdapter(@NonNull Context context, int resource, @NonNull List<Category> objects) {
        super(context, resource, objects);
        this.context = context;
        this.categories = objects;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = layoutInflater.inflate(R.layout.rowcategory, parent, false);

        TextView nameCategory = (TextView) rowView.findViewById(R.id.nameCategory);

        nameCategory.setText(categories.get(position).getName());

        return rowView;
    }
}
