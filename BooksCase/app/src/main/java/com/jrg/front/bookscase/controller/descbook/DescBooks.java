package com.jrg.front.bookscase.controller.descbook;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bumptech.glide.Glide;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentDescBooksBinding;
import com.jrg.front.bookscase.entity.Book;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.entity.CartInfo;
import com.jrg.front.bookscase.entity.CartLineInfo;
import com.jrg.front.bookscase.entity.segurity.enums.RolName;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DescBooks extends Fragment {

    private APIService apiService;
    private Book book;
    private FragmentDescBooksBinding binding;
    private SharedPrefManager sharedPrefManager;
    private CartInfo cartInfo;
    private int quantity;



    public DescBooks() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String isbn = null;
        binding = FragmentDescBooksBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Bundle bundle = getArguments();
        sharedPrefManager = new SharedPrefManager(getContext().getApplicationContext());

        if(bundle != null){
            isbn = (String) bundle.getString("isbn");
        }
        if(sharedPrefManager.isLoggedIn() && Utils.hasRol(sharedPrefManager.getUser().getUser(), RolName.ROLE_ADMIN)){
            binding.editarBook.setVisibility(Button.VISIBLE);
        }else{
            binding.editarBook.setVisibility(Button.INVISIBLE);
        }

        long isbnl = Long.valueOf(isbn);
        fillBook(isbnl,root);



        sharedPrefManager.getCartInfo().findLineByISBN(isbnl);


        CartLineInfo cartLineInfo = sharedPrefManager.getCartInfo().findLineByISBN(isbnl);
        cartInfo = sharedPrefManager.getCartInfo();

        quantity =  (cartLineInfo!=null)?cartLineInfo.getQuantity():0;
        binding.idDescQuantybook.setText(String.valueOf(quantity));
        binding.seguirComprando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = Integer.valueOf(binding.idDescQuantybook.getText().toString());

                if(sharedPrefManager.getCartInfo().isFindLineByISBN(isbnl)){
                    cartInfo.updateBook(isbnl, quantity);
                }else{
                    cartInfo.addBook(book, quantity);
                }

                sharedPrefManager.saveCart(cartInfo);

                NavHostFragment.findNavController(DescBooks.this)
                        .navigate(R.id.action_desc_book_to_action_nav_home);
            }
        });

        binding.irCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DescBooks.this)
                        .navigate(R.id.action_desc_book_to_cart);
            }
        });

        binding.idDescQuantybook.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                quantity = Integer.valueOf(binding.idDescQuantybook.getText().toString());

                if(sharedPrefManager.getCartInfo().isFindLineByISBN(isbnl)){
                    cartInfo.updateBook(isbnl, quantity);
                }else{
                    cartInfo.addBook(book, quantity);
                }

                sharedPrefManager.saveCart(cartInfo);

            }
        });

        binding.idDescAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quatity = Integer.valueOf(binding.idDescQuantybook.getText().toString());
                quatity++;
                binding.idDescQuantybook.setText(String.valueOf(quatity));
            }
        });

        binding.idDescRemoveBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quatity = Integer.valueOf(binding.idDescQuantybook.getText().toString());
                quatity--;
                Log.d("DescBooks", "Valor quatity" + quatity);
                if(quatity <= 0){
                    quatity = 0;
                }
                binding.idDescQuantybook.setText(String.valueOf(quatity));
            }
        });

        binding.editarBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle args = new Bundle();

                args.putSerializable("book", book);

                NavHostFragment.findNavController(DescBooks.this)
                        .navigate(R.id.action_desc_book_to_newBook, args);

            }
        });


        return root;
    }

    public void fillBook(long isbn, View root){
        apiService = Apis.getClient();

        Call<Book> call = apiService.getBook(isbn);

        call.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                book = response.body();
                Log.d("DescBooks", book.getName());


                Glide.with(root).load(Apis.getUrlImg(book.getPath_img())).into(binding.idDescPortada);
                binding.idDescTitle.setText(book.getName());
                binding.idDescAuthor.setText(book.getAuthors().iterator().next().getName() + " " + book.getAuthors().iterator().next().getSurnames());
                binding.idDescISBN.setText(String.valueOf(book.getIsbn()));
                binding.idDescBinding.setText(book.getBinding());
                binding.idDescLanguage.setText(book.getLanguage().getLanguage());
                binding.idDescCategoria.setText(book.getCategories().iterator().next().getName());
                binding.idDescNPag.setText("Nº Pag: " + book.getN_pag());
                binding.idDescSumary.setText(book.getSummary());
                binding.idDescPrice.setText(String.valueOf(book.getPrice()) + "€");

            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {

            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}