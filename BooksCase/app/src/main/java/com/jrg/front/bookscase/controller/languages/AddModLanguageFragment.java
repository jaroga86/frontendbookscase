package com.jrg.front.bookscase.controller.languages;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentAddModLanguageBinding;
import com.jrg.front.bookscase.entity.Language;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.dto.LanguageDto;
import com.jrg.front.bookscase.dto.Mensaje;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddModLanguageFragment extends Fragment {

    private FragmentAddModLanguageBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private Language language;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAddModLanguageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());
        apiService = Apis.getClient();

        Bundle bundle = getArguments();

        if(bundle != null){
            language = (Language) bundle.getSerializable("language");
            binding.editLanguage.setText(language.getLanguage());
            binding.idSaveLanguage.setVisibility(Button.INVISIBLE);
        }else{
            binding.idModificarLanguage.setVisibility(Button.INVISIBLE);
            binding.idEliminarLanguage.setVisibility(Button.INVISIBLE);
        }

        binding.idSaveLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Call<Mensaje> call = apiService.postLanguage(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                        new LanguageDto(binding.editLanguage.getText().toString()));

                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                        responseClient(response);
                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });

            }
        });

        binding.idModificarLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Mensaje> call = apiService.updateLanguage(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                        language.getId(), new LanguageDto(binding.editLanguage.getText().toString()));
                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                        responseClient(response);
                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });
            }
        });

        binding.idEliminarLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Mensaje> call = apiService.deleteLanguage(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                        language.getId());
                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                        responseClient(response);
                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });
            }
        });


        return root;
    }


    protected void responseClient(Response<Mensaje> response){

        Mensaje mensaje;

        if(!response.isSuccessful() && response.code() == 400){
            try{
                Gson gson = new Gson();
                String jsonError = response.errorBody().string();
                mensaje = gson.fromJson(jsonError, Mensaje.class);
                Toast.makeText(getContext().getApplicationContext(),  Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();
            }catch (IOException io){
                io.printStackTrace();
            }

        }else if(response.code() == 200){

            mensaje = response.body();


            Toast.makeText(getContext().getApplicationContext(), Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();

            NavHostFragment.findNavController(AddModLanguageFragment.this)
                    .navigate(R.id.action_addModLanguageFragment_to_nav_language);
        }
    }
}