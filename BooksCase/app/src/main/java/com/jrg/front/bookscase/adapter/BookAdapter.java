package com.jrg.front.bookscase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.bumptech.glide.Glide;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.entity.Book;
import com.jrg.front.bookscase.util.Apis;

import java.util.List;

public class BookAdapter extends ArrayAdapter<Book> {

    private Context context;
    private List<Book> books;

    public BookAdapter(@NonNull Context context, int resource, @NonNull List<Book> objects) {
        super(context, resource, objects);
        this.context = context;
        this.books = objects;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = layoutInflater.inflate(R.layout.rowlayout, parent, false);

        TextView allBookName = (TextView) rowView.findViewById(R.id.idAllBookName);
        TextView allBookAuthor = (TextView) rowView.findViewById(R.id.idAllBookAuthor);
        TextView allBookCategory = (TextView) rowView.findViewById(R.id.idAllBookCategory);
        TextView allBookBinding = (TextView) rowView.findViewById(R.id.idAllBookBinding);
        TextView allBookPrice = (TextView) rowView.findViewById(R.id.idAllBookPrice);
        ImageView allBookPortada = (ImageView) rowView.findViewById(R.id.idAllBookPortada);


        allBookPortada.setContentDescription(String.valueOf(books.get(position).getIsbn()));
        Glide.with(rowView).load(Apis.getUrlImg(books.get(position).getPath_img())).into(allBookPortada);
        allBookName.setText(books.get(position).getName());
        allBookAuthor.setText(books.get(position).getAuthors().iterator().next().getName() + " " + books.get(position).getAuthors().iterator().next().getSurnames());
        allBookPrice.setText(String.valueOf(books.get(position).getPrice()) + "€");
        allBookCategory.setText(books.get(position).getCategories().iterator().next().getName());
        allBookBinding.setText(books.get(position).getBinding());

        return rowView;


    }
}
