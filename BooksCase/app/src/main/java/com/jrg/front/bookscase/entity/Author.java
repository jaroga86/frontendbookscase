package com.jrg.front.bookscase.entity;

import java.io.Serializable;

public class Author implements Serializable {

    private int id;
    private String name;
    private String surnames;


    public Author(int id, String name, String surnames) {
        this.id = id;
        this.name = name;
        this.surnames = surnames;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }
}
