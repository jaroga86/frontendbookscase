package com.jrg.front.bookscase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.entity.Author;

import java.util.List;

public class AuthorAdapter extends ArrayAdapter<Author> {

    private Context context;
    private List<Author> authors;


    public AuthorAdapter(@NonNull Context context, int resource, @NonNull List<Author> objects) {
        super(context, resource, objects);
        this.context = context;
        this.authors = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = layoutInflater.inflate(R.layout.rowauthor, parent, false);

        TextView name = (TextView) rowView.findViewById(R.id.authorName);
        TextView surnames = (TextView) rowView.findViewById(R.id.authorSurnames);

        name.setText(authors.get(position).getName());
        surnames.setText(authors.get(position).getSurnames());


        return rowView;
    }
}
