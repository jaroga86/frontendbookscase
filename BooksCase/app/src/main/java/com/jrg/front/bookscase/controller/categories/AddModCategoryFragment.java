package com.jrg.front.bookscase.controller.categories;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.databinding.FragmentAddModCategoryBinding;
import com.jrg.front.bookscase.entity.Category;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.dto.CategoryDto;
import com.jrg.front.bookscase.dto.Mensaje;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddModCategoryFragment extends Fragment {


    private FragmentAddModCategoryBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private Category category;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAddModCategoryBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());
        apiService = Apis.getClient();

        Bundle bundle = getArguments();

        if(bundle != null){
            category = (Category) bundle.getSerializable("category");
            binding.editCategoryName.setText(category.getName());
            binding.idSaveCategory.setVisibility(Button.INVISIBLE);
        }else{
            binding.idModificarCategory.setVisibility(Button.INVISIBLE);
            binding.idEliminarCategory.setVisibility(Button.INVISIBLE);
        }

        binding.idSaveCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Call<Mensaje> call = apiService.postCategory(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                        new CategoryDto(binding.editCategoryName.getText().toString()));

                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                        responseClient(response);

                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });


            }
        });

        binding.idModificarCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Mensaje> call = apiService.updateCategory(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(),
                        category.getId(), new CategoryDto(binding.editCategoryName.getText().toString()));
                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                        responseClient(response);
                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });
            }
        });

        binding.idEliminarCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Mensaje> call = apiService.deleteCategory(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken(), category.getId());

                call.enqueue(new Callback<Mensaje>() {
                    @Override
                    public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                        responseClient(response);
                    }

                    @Override
                    public void onFailure(Call<Mensaje> call, Throwable t) {

                    }
                });

            }
        });


        // Inflate the layout for this fragment
        return root;
    }

    protected void responseClient(Response<Mensaje> response){

        Mensaje mensaje;

        if(!response.isSuccessful() && response.code() == 400){
            try{
                Gson gson = new Gson();
                String jsonError = response.errorBody().string();
                mensaje = gson.fromJson(jsonError, Mensaje.class);
                Toast.makeText(getContext().getApplicationContext(),  Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();
            }catch (IOException io){
                io.printStackTrace();
            }

        }else if(response.code() == 200){

            mensaje = response.body();


            Toast.makeText(getContext().getApplicationContext(), Utils.upperCaseFirst(mensaje.getMensaje()), Toast.LENGTH_LONG).show();

            NavHostFragment.findNavController(AddModCategoryFragment.this)
                    .navigate(R.id.action_addModCategoryFragment_to_nav_category);
        }
    }

}