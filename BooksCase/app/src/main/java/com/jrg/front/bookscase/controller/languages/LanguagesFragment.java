package com.jrg.front.bookscase.controller.languages;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.adapter.LanguageAdapter;
import com.jrg.front.bookscase.databinding.FragmentLanguagesBinding;
import com.jrg.front.bookscase.entity.Language;
import com.jrg.front.bookscase.interfaces.APIService;
import com.jrg.front.bookscase.entity.segurity.enums.RolName;;
import com.jrg.front.bookscase.util.Apis;
import com.jrg.front.bookscase.util.SharedPrefManager;
import com.jrg.front.bookscase.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LanguagesFragment extends Fragment {

    private FragmentLanguagesBinding binding;
    private APIService apiService;
    private SharedPrefManager sharedPrefManager;
    private List<Language> listaLanguages = new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentLanguagesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        sharedPrefManager = new SharedPrefManager(root.getContext());

        apiService = Apis.getClient();

        Call<List<Language>> call = apiService.getLanguages(sharedPrefManager.getUser().getBearer() + " " + sharedPrefManager.getUser().getToken());

        call.enqueue(new Callback<List<Language>>() {
            @Override
            public void onResponse(Call<List<Language>> call, Response<List<Language>> response) {
                listaLanguages = response.body();
                binding.idAllLanguage.setAdapter(new LanguageAdapter(root.getContext(), R.layout.rowlanguage, listaLanguages));
            }

            @Override
            public void onFailure(Call<List<Language>> call, Throwable t) {

                Log.e("Language:", t.getMessage());

            }
        });

        binding.idAllLanguage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle args = new Bundle();

                args.putSerializable("language", listaLanguages.get(i));

                NavHostFragment.findNavController(LanguagesFragment.this)
                        .navigate(R.id.action_nav_language_to_addModLanguageFragment, args);
            }
        });

        if(sharedPrefManager.getUser()!=null){
            if(Utils.hasRol(sharedPrefManager.getUser().getUser(), RolName.ROLE_ADMIN)){
                binding.addLanguage.setVisibility(Button.VISIBLE);
            }else{
                binding.addLanguage.setVisibility(Button.INVISIBLE);
            }
        }

        binding.addLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(LanguagesFragment.this)
                        .navigate(R.id.action_nav_language_to_addModLanguageFragment);
            }
        });


        // Inflate the layout for this fragment
        return root;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}