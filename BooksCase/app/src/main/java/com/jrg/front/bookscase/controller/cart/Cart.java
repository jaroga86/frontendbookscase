package com.jrg.front.bookscase.controller.cart;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.jrg.front.bookscase.R;
import com.jrg.front.bookscase.adapter.CartInfoAdapter;
import com.jrg.front.bookscase.databinding.FragmentCartBinding;
import com.jrg.front.bookscase.entity.CartInfo;
import com.jrg.front.bookscase.entity.CartLineInfo;
import com.jrg.front.bookscase.util.SharedPrefManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class Cart extends Fragment {

    private List<CartLineInfo> cartInfoLists = new ArrayList<CartLineInfo>();
    private FragmentCartBinding binding;
    private SharedPrefManager sharedPrefManager;
    private CartInfo cartInfo;
    private static final DecimalFormat df = new DecimalFormat("0.00");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentCartBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        // Inflate the layout for this fragment
        sharedPrefManager = new SharedPrefManager(getContext().getApplicationContext());


        if(!sharedPrefManager.isLoggedIn()){

            NavHostFragment.findNavController(Cart.this)
                    .navigate(R.id.action_cart_to_nav_login);
        }
        cartInfoLists = sharedPrefManager.getCartInfo().getCartLines();
        cartInfo = sharedPrefManager.getCartInfo();

        if(cartInfo.getCartLines().size() == 0){
            binding.nextToAddress.setVisibility(TextView.INVISIBLE);
        }else{
            binding.nextToAddress.setVisibility(TextView.VISIBLE);
        }
        binding.idSpCart.setAdapter(new CartInfoAdapter(root.getContext(), R.layout.rowcart, cartInfoLists));
        binding.idSpCart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle args = new Bundle();
                args.putString("isbn", (String) String.valueOf(cartInfoLists.get(i).getBook().getIsbn()));
                NavHostFragment.findNavController(Cart.this)
                        .navigate(R.id.action_cart_to_desc_book, args);
            }
        });

        binding.idTotalPrice.setText("Precio Total: " + String.valueOf(df.format(cartInfo.getTotalPrice())) +"€");

        binding.nextToAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                NavHostFragment.findNavController(Cart.this)
                        .navigate(R.id.action_cart_to_confirmAddress);
            }
        });
        return root;
    }
}